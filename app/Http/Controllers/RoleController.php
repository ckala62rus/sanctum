<?php

namespace App\Http\Controllers;

use App\Http\Resources\Role\RoleResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function syncRoleUser(Request $request)
    {
        $data = $request->all();

        $roles = Role::whereIn('id', $data['roles_id'])
            ->where('name', '!=', 'Super Admin')
            ->pluck('id');

        $user = User::where('id', $data['user_id'])->first();

        $res = $user->syncRoles($roles);

        return response()->json($res, JsonResponse::HTTP_OK);
    }

    /**
     * Get all roles in system
     * @return JsonResponse
     */
    public function getRoles(): JsonResponse
    {
        $all_roles_in_database = Role::all();

        return response()->json([
            'roles' => RoleResource::collection($all_roles_in_database)
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Get all roles from user by user_id
     * @param Request $request
     * @return JsonResponse
     */
    public function getRoleUserById(Request $request): JsonResponse
    {
        $data = $request->all();

        $user = User::where('id', $data['user_id'])->with('roles')->first();

        return response()->json([
            'user' => $user
        ], JsonResponse::HTTP_OK);
    }
}
