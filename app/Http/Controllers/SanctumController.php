<?php

namespace App\Http\Controllers;

use App\Http\Requests\MobileUser\MobileUserGetSecretRequest;
use App\Models\MobileUser;
use App\Repositories\MobileUserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mockery\Exception;

class SanctumController extends Controller
{
    /**
     * @var MobileUserRepository
     */
    private MobileUserRepository $mobileUserRepository;

    /**
     * SanctumController constructor.
     * @param MobileUserRepository $mobileUserRepository
     */
    public function __construct(
        MobileUserRepository $mobileUserRepository
    ) {
        $this->mobileUserRepository = $mobileUserRepository;
    }

    /**
     * Get SMS with code
     * @param MobileUserGetSecretRequest $request
     * @return JsonResponse
     */
    public function getCodeSMS(MobileUserGetSecretRequest $request)
    {
        $data = $request->all();

        try {
            if (isset($data['phone_number'])) {

                $isExistUser = MobileUser::where('phone_number', $data['phone_number'])->first();

                if (!$isExistUser) {
                    $phone = MobileUser::create([
                        'phone_number' => $data['phone_number'],
                        'secret' => Str::random(10),
                    ]);

                    return response()->json($phone, JsonResponse::HTTP_CREATED);
                }

                $updateMobileUserModel = $this
                    ->mobileUserRepository
                    ->update([
                        'secret' => Str::random(10)
                    ],$isExistUser->id);

                return response()->json($updateMobileUserModel, JsonResponse::HTTP_CREATED);
            }
        } catch (Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], JsonResponse::HTTP_NOT_FOUND);
        }
    }

    /**
     * Set token by phone number and secret code
     * @param Request $request
     * @return JsonResponse
     */
    public function getTokenByCode(Request $request): JsonResponse
    {
        $data = $request->all();

        $phone = MobileUser::where('phone_number', $data['phone'])
            ->where('secret', $data['secret'])
            ->first();

        if ($phone) {
            $res = $phone->createToken('token-name', ['server:update'])->plainTextToken;
            return response()->json($res, JsonResponse::HTTP_OK);
        }

        return response()->json([], JsonResponse::HTTP_NOT_FOUND);
    }

    /**
     * Get authorization user info
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        $currentUser = auth()->user();

        return response()->json($currentUser, JsonResponse::HTTP_OK);
    }
}
