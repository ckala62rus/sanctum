<?php

namespace App\Repositories;

use App\Models\MobileUser;

class MobileUserRepository extends Repository
{
    /**
     * MobileUserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new MobileUser();
    }
}
