<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class CreateUserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperAdmin = Role::create(['name' => 'Super Admin']);
        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleManager = Role::create(['name' => 'Manager']);
        $roleOperator = Role::create(['name' => 'Operator']);

        $owner = User::create([
            'name' => 'Owner',
            'email' => 'owner@mail.ru',
            'password' => Hash::make(123123)
        ]);
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@mail.ru',
            'password' => Hash::make(123123)
        ]);
        $manager = User::create([
            'name' => 'Manager',
            'email' => 'manager@mail.ru',
            'password' => Hash::make(123123)
        ]);
        $operator = User::create([
            'name' => 'Operator',
            'email' => 'operator@mail.ru',
            'password' => Hash::make(123123)
        ]);
        $owner->assignRole('Super Admin');
        $admin->assignRole('Admin');
        $manager->assignRole('Manager');
        $operator->assignRole('Operator');
    }
}
