<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/* for mobile authorize */
Route::post('tokens/sms-code', [\App\Http\Controllers\SanctumController::class, 'getCodeSMS']);
Route::post('tokens/set-token', [\App\Http\Controllers\SanctumController::class, 'getTokenByCode']);

Route::post('tokens/me', [\App\Http\Controllers\SanctumController::class, 'me'])
    ->middleware(['auth:sanctum']);

Route::group(['middleware' => ['role:Super Admin']], function () {
    Route::get('test/super-admin', function () {
        return response()->json(['data' => 'super-admin permission'], \Illuminate\Http\JsonResponse::HTTP_OK);
    });
});

Route::group(['middleware' => ['role:Super Admin|Admin']], function () {
    Route::get('test/admin', function () {
        return response()->json(['data' => 'admin permission'], \Illuminate\Http\JsonResponse::HTTP_OK);
    });
    Route::post('role/sync', [\App\Http\Controllers\RoleController::class, 'syncRoleUser']);
    Route::post('role/roles', [\App\Http\Controllers\RoleController::class, 'getRoles']);
    Route::post('role/user/roles', [\App\Http\Controllers\RoleController::class, 'getRoleUserById']);
});

Route::group(['middleware' => ['role:Manager|Admin|Super Admin']], function () {
    Route::get('test/manager', function () {
        return response()->json(['data' => 'manager permission'], \Illuminate\Http\JsonResponse::HTTP_OK);
    });
});

Route::group(['middleware' => ['role:Operator|Admin|Super Admin']], function () {
    Route::get('test/operator', function () {
        return response()->json(['data' => 'operator permission'], \Illuminate\Http\JsonResponse::HTTP_OK);
    });
});
